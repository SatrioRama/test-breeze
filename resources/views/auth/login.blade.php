@extends('auth.layout')

@section('title', 'Login')

@section('content')
<form action="{{ route('login') }}" method="post">
    @csrf
    <div class="row">
        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
        <div class="col-lg-6">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control form-control-user" name="email"
                        id="exampleInputEmail" aria-describedby="emailHelp"
                        placeholder="Enter Email Address...">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control form-control-user" name="password"
                        id="exampleInputPassword" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="text">Login</span>
                </button>

                <div class="text-center">
                    <a class="small" href="{{route('register')}}">Create an Account!</a>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
